package adma.core;

import java.util.Set;

import adma.core.devices.Device;
import service.discovery.ServiceDiscovery;


public class Adma {
	private Set<Device> devices;
	private ServiceDiscovery service;

	private final String path = "devices/";

	public Adma() {
		setServices();
		loadDevices();
	}

	private void setServices() {
		service = new ServiceDiscovery();
	}

	private void loadDevices() {
		devices = service.findClasses(path);
	}

	public Set<Device> getDevices() throws Exception {
		return devices;
	}
}
