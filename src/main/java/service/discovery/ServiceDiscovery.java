package service.discovery;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

import adma.core.devices.Device;

public class ServiceDiscovery {

	public Set<Device> findClasses(String path) {
		Set<Device> devices = new HashSet<>();

		for (File file : new File(path).listFiles()) {
			if (!file.getName().endsWith(".class")) {
				continue;
			}

			Class<?> newClass = null;

			try {
				String fileName = file.getName().substring(0, file.getName().lastIndexOf("."));

				URL[] urls = { new File(path).toURI().toURL() };
				URLClassLoader urlClassLoader = new URLClassLoader(urls);

				newClass = urlClassLoader.loadClass(fileName);
				urlClassLoader.close();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}

			if (!Device.class.isAssignableFrom(newClass)) {
				throw new RuntimeException();
			}

			try {
				Constructor<?> constructor = newClass.getConstructor();
				Device dev = (Device) constructor.newInstance();
			
				devices.add(dev);
			} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
					| IllegalArgumentException | InvocationTargetException e) {
				e.printStackTrace();
			}
		}
		return devices;
	}
	
	
}
